package limit

import (
	"math/rand"
	"testing"
	"time"
)

func BenchmarkBandWidthLimit(b *testing.B) {
	limiter := NewBandwidthlimiter(0)
	rand.Seed(time.Now().UnixNano())
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			//byteNum := uint32(rand.Intn(5242880-500)) + 500
			byteNum := uint32(1<<32 - 2)
			_ = limiter.ApplyForResources(byteNum)
			time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
			//nolint:staticcheck
			limiter.ReleaseResource(byteNum)
		}
	})
}
