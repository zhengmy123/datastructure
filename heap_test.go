package main

import (
	"fmt"
	"testing"
)

func TestNewHeap(t *testing.T) {
	/* obj := Constructor()
	obj.Push(-2)
	obj.Push(1)
	obj.Push(-3)
	obj.Pop()
	param_3 := obj.Top()
	param_4 := obj.GetMin()
	fmt.Println(param_3, param_4) */
	heap := NewHeap(false)
	heap.Push(1)
	heap.Push(3)
	heap.Push(2)
	heap.Push(1)
	heap.Print()
	fmt.Println(heap.Pop())
	heap.Print()
	fmt.Println(heap.Pop())
	heap.Print()
	heap.Push(10)
	heap.Push(1)
	heap.Push(20)
	heap.Print()
	fmt.Println(-1 << 31)
}
