package main

import (
	"fmt"
)

type Context struct {
	Name string `json:"name"`
}
type middlewareFunc func(HandlerFunc) HandlerFunc

type HandlerFunc func(Context) error

func mid1(next HandlerFunc) HandlerFunc {
	return func(c Context) error {
		fmt.Println("middleware： mid1 ", c.Name)
		return next(c)
	}
}
func mid2(next HandlerFunc) HandlerFunc {
	return func(c Context) error {
		fmt.Println("middleware： mid2 ", c.Name)
		return next(c)
	}
}
func mid3(next HandlerFunc) HandlerFunc {
	return func(c Context) error {
		fmt.Println("middleware： mid3 ", c.Name)
		return next(c)
	}
}
func main() {
	list := []middlewareFunc{mid1, mid2, mid3}
	h := func(c Context) error {
		fmt.Println("处理函数", c.Name)
		return nil
	}
	for i := len(list) - 1; i >= 0; i-- {
		h = list[i](h)
	}
	e := Context{
		Name: "张三",
	}
	err := h(e)
	if err != nil {
		fmt.Println("error:", err.Error())
	}
	fmt.Println("完成")
}
