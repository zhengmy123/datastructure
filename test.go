package main

const (
	Max = 1<<31 - 1
	Min = -1 << 31
)

func reverseInt(x int) int {
	var res int
	for x != 0 {
		res = res*10 + x%10
		x = x / 10
	}
	if res > Max || res < Min {
		return 0
	}
	return res
}
